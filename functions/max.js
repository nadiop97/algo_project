function maxDifference(array) {
  let maxDiff = 0;
  if(array == null) {
    return false
  } else {
    for(i=1; i < array.length; i++) {
      if((array[i]-array[i-1]) > maxDiff) {
        maxDiff = array[i]-array[i-1]
      }
  }
  return maxDiff
  }
}

// let array = [13,3,50,10,15]
// console.log(array, maxDifference(array))

module.exports = maxDifference

