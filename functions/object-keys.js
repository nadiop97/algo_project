function objectKeys(object) {
  
  if(!object || (typeof object[0]=== "string") ) return false

    let arrayKeys = []
      for(key in object) {
        arrayKeys.push(key)
      }
    return arrayKeys
}

module.exports = objectKeys