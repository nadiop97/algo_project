class Vehicule {
  constructor(model, price) {
    this.model = model
    this.price = price
  }

  toString() {
    return 'Model: '+this.model+', Price: '+this.price
  }

}

let vehicule = new Vehicule('model','price')
console.log(vehicule)
console.log(vehicule.toString())
console.log(" ")

class Car extends Vehicule {
  constructor(model, price) {
    super(model, price)
  }

  start() {
    return 'La voiture démarre'
  }

  speedUp() {
  return 'La voiture accélére'
  }

}

let car = new Car('peugeot','2M')
console.log(car)
console.log(car.start())
console.log(car.speedUp())
console.log(car.toString())
console.log(" ")


class Truck extends Vehicule {
  constructor(model, price) {
    super(model, price)
  }

  start() {
    return 'Le camion démarre'
  }

  speedUp() {
  return 'Le camion accélére'
  }


}

let camion = new Truck('MAN','500000')
console.log(camion)
console.log(camion.start())
console.log(camion.speedUp())
console.log(camion.toString())


module.exports = { Truck, Car, Vehicule }