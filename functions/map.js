function map(array, callback) {
  if(!array && !callback) return false
  let array2 = []
  for(let i = 0; i < array.length ; i++) {
    let result = callback(array[i])
    array2.push(result)
  }
  return array2
}

console.log(map([1, 2, 3, 4], function (number) {
  return number * 2;
}))

module.exports = map

