function containsEquals(tab) {
  let equal = 0

  if(tab != null) {  
      for(let i=0; i < tab.length; i++) {
        for(let j=i+1; j < tab.length; j++) {
          if( tab[i]==tab[j]) {
            equal = tab[i]
          }
        }
      }
      return equal
    } else {
      return false
    }
}

// let tab = [1, 3, 'test', 10, 'test']


// if(tab != null) {  
//   for(let i=0; i < tab.length; i++) {
//     for(let j=i+1; j < tab.length; j++) {
//       if( tab[i]==tab[j]) {
//         equal = tab[i]
//       }
//     }
//   }
//   console.log(equal)
// } else {
//   console.log(false)
// }
// console.log(containsEquals(tab))


module.exports = containsEquals