class User {
  //constructor
  constructor(firstName, lastName) {
    this.firstName = firstName
    this.lastName = lastName
  }

  //methods
  spellName() {
    return 'My name is '  + this.firstName + ' ' + this.lastName
  }
}  

module.exports = User