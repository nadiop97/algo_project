function maxFullDifference(tab) {
  if(!tab) return false
  
  let max = tab[0]
  let min = tab[0]
  let isAllString = true

  for(let i = 0; i<tab.length;i++) {
    if(!Number.isInteger(tab[i])) {
      continue
    }
    else {
      isAllString = false
    }
    if(tab[i]<min) {
        min = tab[i]
      } 
      if(tab[i]>max){
        max = tab[i]
      }
  }
  if(isAllString) {
    return false
  }
  return max-min
}




    





module.exports = maxFullDifference