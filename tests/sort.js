const { assert, expect } = require('chai');
const { describe, it } = require('mocha');

const sort = require('./../functions/sort')

module.exports = describe('#4 - Recoder la fonction sort de Javascript', function () {
  it('return the sorted array', function () {
    expect(sort([1, 3, 2, 5, 4])).to.eqls([1, 2, 3, 4, 5])
    expect(sort(['a', 'c', 'b', 'z', 'h'])).to.eqls(['a', 'b', 'c', 'h', 'z'])
  });

  it('should return false', function () {
    assert.equal(sort([]), false)
    assert.equal(sort(null), false)
    assert.equal(sort(undefined), false)
  });

});